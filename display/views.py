from django.shortcuts import render, redirect
from django.contrib import messages

from guestact.models import GuestbookItem


# Index (/)
def index(request):
    return render(request, "index.html", {})


# Credits (/credits)
def credits(request):
    return render(request, "credits.html", {})


# Contact form (/contact)
def contact(request):
    msg = messages.get_messages(request)
    return render(request, "contact.html", {"messages": msg})


# Guestbook results (/guestbook)
def guestbook(request):
    posts = sorted(GuestbookItem.objects.all(), key=lambda x: x.date, reverse=True)
    return render(request, "guestbook.html", {"posts": posts})


# Personal pages (/personal/<name>)
def personal(request, name):
    p_pages = ["erwin", "hans", "kevin", "nick"]
    if name.lower() in p_pages:
        return render(request, "personal/" + name.lower() + ".html", {})
    else:
        return redirect("index")


# Forced requirements
def requirements(request):
    return render(request, "requirements.html", {})
