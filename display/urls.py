from django.conf.urls import url

from display import views

urlpatterns = [
    url(r'^requirements_forced', views.requirements, name="requirements"),
    url(r'^contact', views.contact, name="contact"),
    url(r'^guestbook', views.guestbook, name="guestbook"),
    url(r'^personal/(.*)', views.personal, name="personal"),
    url(r'^credits', views.credits, name="credits"),
    url(r'^', views.index, name="index"),
]
