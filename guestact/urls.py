from django.conf.urls import url
from guestact import views

urlpatterns = [
    url(r'^', views.translate, name="translate_req"),
]
