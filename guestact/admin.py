from django.contrib import admin
from guestact.models import ContactItem, GuestbookItem


class ContactItemAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'email', 'message']


class GuestbookItemAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'age', 'date', 'message']

# Register your models here.
admin.site.register(ContactItem, ContactItemAdmin)
admin.site.register(GuestbookItem, GuestbookItemAdmin)
