from django.db import models


# Create your models here.
class ContactItem(models.Model):
    name = models.CharField(max_length=30, editable=False)
    email = models.EmailField(max_length=100, editable=False)
    message = models.CharField(max_length=500, editable=False)

    def __str__(self):
        return self.email


class GuestbookItem(models.Model):
    name = models.CharField(max_length=30, editable=False)
    age = models.IntegerField(editable=False)
    message = models.CharField(max_length=100, editable=False)
    date = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.name
