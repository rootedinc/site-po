from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.shortcuts import render, redirect

from guestact.models import ContactItem, GuestbookItem


# Translate the url from the regex to parsable code (something/submit)
def translate(request, from_form):
    from_form = from_form.lower()
    if from_form == "contact":
        return contact_proc(request)
    elif from_form == "guestbook":
        return guestbook_proc(request)
    else:
        return render(request, "easteregg.html", {})


# Contact form processing (/contact/submit)
def contact_proc(request):
    if request.method == "POST":

        name = request.POST.get("name")
        email = request.POST.get("email")
        message = request.POST.get("message")

        valid = check_post(name=name, email=email, message=message)

        if not valid:
            messages.error(request, "Invalid entry, please try again!")
            return redirect("contact")
        else:
            contact = ContactItem(name=name, email=email, message=message)
            contact.save()
            messages.success(request, "Your contact inquiry has successfully reached us.")
            return redirect("contact")
    else:
        return redirect("contact")


# Guestbook form processing (/guestbook/submit)
def guestbook_proc(request):
    if request.method == "POST":
        if "g_filled" not in list(request.session.keys()):
            g_filled = False
        else:
            g_filled = request.session["g_filled"]

        if g_filled:
            return redirect("guestbook")

        name = request.POST.get("name")
        age = request.POST.get("age")
        message = request.POST.get("message")

        valid = check_post(name=name, age=age, message=message)
        age = int(age)

        if not valid:
            messages.error(request, "Invalid entry, please try again!")
            return redirect("guestbook")
        else:
            guestbook = GuestbookItem(name=name, age=age, message=message)
            guestbook.save()

            request.session["g_filled"] = True

            return redirect("guestbook")
    else:
        return redirect("guestbook")


def check_post(name=None, age=None, email=None, message=None):
    if name is not None:
        if name == "" or len(name) > 30:
            return False

        for char in name:
            if char == ' ':
                continue

            if not char.isalpha():
                return False

    if age is not None:
        if len(age) > 3:
            return False

        for char in age:
            if not char.isdigit():
                return False

    if email is not None:
        if len(email) > 100:
            return False

        try:
            validate_email(email)
        except ValidationError:
            return False

    if message is not None:
        if message == "" or len(message) > 500:
            return False

    return True
