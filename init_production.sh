#!/bin/bash

sed -i 's/DEBUG\ \=\ True/DEBUG\ \=\ False/ig' sitepo/settings.py
sed -i "s/ALLOWED_HOSTS.*/ALLOWED_HOSTS\ \=\ \[\'\*\'\]/ig" sitepo/settings.py
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic --no-input
